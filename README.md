# Sample DAO project

### ERC-20 contract
https://rinkeby.etherscan.io/address/0xC06F7b5cdAccd18A37639FdceCE943e7eD744c7E

### DAO contract
https://rinkeby.etherscan.io/address/0x041361172cA855d1863BaB7eF0fBdec63677fCEF


## Testing and coverage log:

    Deposit & withdraw
      ✔ Depositing by 20 participants (822ms)
      ✔ Withdraw should be success
      ✔ Front-running withdraw should be reverted
    Add proposal
      ✔ Adding proposal #1
      ✔ Adding proposal #2
      ✔ Adding proposal #3
      ✔ Adding proposal #4 (77ms)
    Vote
      ✔ Proposal #1. Voting by 3 participants
        Proposal #1. YES votes 600
        Proposal #1. NO votes 0
      ✔ Proposal #1. Checking votes amount
      ✔ Proposal #2. Voting by 6 participants (73ms)
        Proposal #2. YES votes 2100
        Proposal #2. NO votes 0
      ✔ Proposal #2. Checking votes amount
      ✔ Proposal #3. Voting by 9 participants (107ms)
        Proposal #3. YES votes 2800
        Proposal #3. NO votes 1700
      ✔ Proposal #3. Checking votes amount
      ✔ Proposal #4. Voting by 12 participants (143ms)
        Proposal #4. YES votes 2800
        Proposal #4. NO votes 5000
      ✔ Proposal #4. Checking votes amount
      ✔ Withdraw by voter before proposal ending should be reverted
      ✔ Voting without deposits should be reverted
      ✔ Voting for non-exist proposal should be reverted
      ✔ Second voting should be reverted
      ✔ Too early finishing should be reverted
      ✔ Too late voting should be reverted
    Finish
      ✔ Proposal without quorum should by cancelled
      ✔ Second finishing should be reverted
      ✔ Finishing should call recipient, call should be success
      ✔ Finishing should call recipient, call should be failed
      ✔ Proposal with minority of YES votes should be cancelled


  26 passing (2s)

File          |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
--------------|----------|----------|----------|----------|----------------|
 contracts/   |      100 |      100 |      100 |      100 |                |
  DAO.sol     |      100 |      100 |      100 |      100 |                |
  Token20.sol |      100 |      100 |      100 |      100 |                |
--------------|----------|----------|----------|----------|----------------|
All files     |      100 |      100 |      100 |      100 |                |
--------------|----------|----------|----------|----------|----------------|

